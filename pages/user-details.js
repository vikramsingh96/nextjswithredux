import React, { Component } from 'react';
import Navbar from '../components/NavBar';
import Head from 'next/head';
import { connect } from 'react-redux';
import { GET_USER_DETAILS } from '../redux/actions/actions';

class UserDetails extends Component {

    componentDidMount() {
        this.props.handleGetUserDetails();
    }
    
    render() {
        return (
            <React.Fragment>
                <Head>
                    <title>Details</title>
                    <link rel="icon" href="/favicon.ico" />
                </Head>
                <Navbar background="#343a40"/>
                <div className="details_container">
                      <div className="inner_container">
                         <input type="text" name="name" value={this.props.userData.name} disabled/>
                         <input type="text" name="email" value={this.props.userData.email} disabled/>
                         <input type="text" name="subject" value={this.props.userData.subject} disabled/>
                         <input type="text" name="message" value={this.props.userData.message} disabled/>
                      </div>  
                </div>
                
                <style>
                    {`
                        .details_container
                        {
                            height:auto;
                            width:100%;
                            padding:100px 0px 100px 0px;
                        }
                        .inner_container
                        {
                            height:auto;
                            width:50%;
                            margin:0 auto;
                            padding:10px 40px 10px 10px;
                            background:green;
                            box-sizing:border-box;
                            border-radius:3px;
                            background:#fff;
                        }
                        .inner_container input
                        {
                            height:45px;
                            width:100%;
                            background:#fff;
                            border:1px solid lightgray;
                            border-radius:5px;
                            outline:none;
                            margin:15px;
                            padding:10px;
                        }
                    `}
                </style>

                <style global jsx>{`
                body {
                  margin:0px !important;
                  padding:0px !important;
                  overflow-x: hidden;
                  background:#eee;
                }
          `}</style>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userData: state.userDetails
    }
}
  

const mapDispatchToProps = (dispatch) =>{
    return {
        handleGetUserDetails: data => dispatch({ type: GET_USER_DETAILS, data })
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(UserDetails)