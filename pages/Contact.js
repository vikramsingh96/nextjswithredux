import React, { Component } from 'react';
import Navbar from '../components/NavBar';
import { connect } from 'react-redux';
import Head from 'next/head';
import { SAVE_USER_DETAILS } from '../redux/actions/actions';
import Router from 'next/router';



class Contact extends Component {

    constructor(props){
        super(props);
        this.state={
            name:'',
            email:'',
            subject:'',
            message:''
        }
    }

    handleOnChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    handleOnSubmit=()=>{
        if(this.state.name==='')
        {
            alert("Enter Your name.");
        }
        else if(this.state.email===''){
            alert("Enter Your email.");
        }
        else if(this.state.subject==='')
        {
            alert("Enter Your subject.");
        }
        else if(this.state.message==='')
        {
            alert("Enter Your message.");
        }
        else
        {
            this.props.handleSaveUserDetails(this.state);
            Router.push('/user-details');
        }
    }

    render() {
        return (
            <React.Fragment>
                <Head>
                    <title>Contact</title>
                    <link rel="icon" href="/favicon.ico" />
                </Head>
                <Navbar background="#343a40"/>
                <div className="contact_container">
                    <div className="header_container">
                        <h1>Contact Me</h1>
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
                    </div>

                     <div className="address_container">
                            <div id="shown_addres_box">
                                <div className="card_container">
                                    <h1>Address</h1>
                                    <p>198 West 21th Street, Suite 721 New York NY 10016</p>
                                </div>   

                                <div className="card_container">
                                    <h1>Contact Number</h1>
                                    <a href="#">+ 1235 2355 98</a>
                                </div>   

                                <div className="card_container">
                                    <h1>Email Address</h1>
                                    <a href="#">info@yoursite.com</a>
                                </div>   

                                <div className="card_container">
                                    <h1>Website</h1>
                                    <a href="#">yoursite.com</a>
                                </div>   
                            </div>
                            


                            <div id="fill_addres_box">
                                <input type="text" name="name" placeholder="Your Name" onChange={this.handleOnChange}/>
                                <input type="text" name="email" placeholder="Your Email" onChange={this.handleOnChange}/>
                                <input type="text" name="subject" placeholder="Subject" onChange={this.handleOnChange}/>
                                <textarea type="text" name="message" placeholder="Message" onChange={this.handleOnChange}/>

                                <button onClick={this.handleOnSubmit}>Send Message</button>
                            </div>
                     </div>   

                </div>

                <style jsx>{`
                    .contact_container{
                        height:auto;
                        width:100%;
                        // background:red;
                        padding-top:5em;
                        padding-bottom:2em;
                        
                    }
                    .header_container
                    {
                        height:auto;
                        width:100%;
                        text-align:center;
                    }
                    .header_container h1
                    {
                        font-size:30px;
                        font-family:sans-serif;
                    }
                    .header_container p
                    {
                        font-size:15px;
                        font-family:sans-serif;
                        color:gray;
                        font-weight:bold;
                    }
                    .address_container
                    {
                        display:flex;
                        flex-direction:row;
                        // flex-wrap: wrap;
                        height:auto;
                        width:100%;
                        padding-top:3em;             
                        justify-content:space-around;    
                        box-sizing: border-box;     
                    }
                    #shown_addres_box
                    {
                        height:auto;
                        width:45%;
                        padding:0px 15px 15px 50px;
                        // background:green;
                        box-sizing: border-box;
                    }
                    #fill_addres_box
                    {
                        height:auto;
                        width:45%;
                        padding:15px 50px 15px 50px;
                        background:maroon;
                        background:#f8f9fa;
                        box-sizing: border-box;   
                    }

                    .card_container
                    {
                        height:auto;
                        width:80%;
                        padding:10px;
                        background:#f8f9fa;
                        margin:10px;
                    }

                    .card_container h1
                    {
                        font-size:18px;
                        font-family:sans-serif;
                    }
                    .card_container p
                    {
                        font-size:13px;
                        font-family:sans-serif;
                        color:gray;
                        font-weight:bold;
                    }
                    .card_container a
                    {
                        font-weight:bold;
                        font-size:14px;
                        font-family:sans-serif;
                    }

                    #fill_addres_box input{
                        height:40px;
                        width:100%;
                        margin-top:15px;
                        border:1px solid lightgray;
                        background:#fff;
                        padding-left:10px;
                        border-radius:5px;
                    }
                    #fill_addres_box textarea
                    {
                        height:100px;
                        width:100%;
                        margin-top:15px;
                        border:1px solid lightgray;
                        background:#fff;
                        padding-left:10px;
                        padding-top:10px;
                        border-radius:5px;
                    }
                    
                    #fill_addres_box button{
                        height:50px;
                        width:200px;
                        margin-top:20px;
                        background:blue;
                        outline:none;
                        cursor:pointer;
                        color:#fff;
                        font-size:13px;
                        border-radius:3px;
                        border:1px solid blue;
                    }
                
                `}</style>

                <style global jsx>{`
                body {
                  margin:0px !important;
                  padding:0px !important;
                  overflow-x: hidden;
                }
          `}</style>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        // isloading: state.isLoading
    }
}
  

const mapDispatchToProps = (dispatch) =>{
    return {
        handleSaveUserDetails: data => dispatch({ type: SAVE_USER_DETAILS, data })
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(Contact)