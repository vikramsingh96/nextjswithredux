import React, { Component } from 'react';
import Head from 'next/head';
import NavBar from '../components/NavBar';
import NavigationBar from '../components/NavigationBar';
import AwesomeSlider from 'react-awesome-slider';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import 'react-awesome-slider/dist/styles.css';

const AutoplaySlider = withAutoplay(AwesomeSlider);


export default class Home extends Component {
  render(){
    return (
      <div className="container">
        <Head>
          <title>Home</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <NavBar background="transparent"/>
        <section className="hero_home_section">
          <h3>
              <span>Welcome</span>
              to MeetUp.
          </h3>
          <NavigationBar/>
          <div className="home-slider_container">
              <div className="slide_inner_container">
                  <AutoplaySlider interval={6000} play={true}
                  cancelOnInteraction={false} >
                  <div style={{backgroundImage:`url('/static/bg_1.jpg')`,backgroundSize:'cover'}}>1</div>
                  <div style={{backgroundImage:`url('/static/bg_2.jpg')`,backgroundSize:'cover'}}>1</div>
                </AutoplaySlider>
              </div>
          </div>

        </section>


         <style jsx>{`
              .container
              {
                height:100vh;
                width:100%;
              }
              .hero_home_section
              {
                height:100%;
                width:100%;
                position:relative;
              }
              .hero_home_section h3 
              {
                line-height:1.5;
                writing-mode: vertical-lr;
                transform: rotate(180deg);
                text-orientation: sideways;
                text-align: center;
                text-orientation: sideways;
                margin: 0;
                position: absolute;
                top: 0;
                left: 3em;
                height: 100%;
                text-transform: uppercase;
                font-weight: 600;
                letter-spacing: 10px;
                font-size: 12px;
                z-index: 100;
                color: rgba(0,0,0,.3);
              }
              .hero_home_section h3 span
              {
                color:#333;
              }

              .home-slider_container
              {
                display:flex;
                height:auto;
                width:100%;
                position:realtive;
                overflow:hidden;
                justify-content:flex-end;
                // background:white;
              }
              .slide_inner_container
              {
                height:100vh;
                width:93%;
                position:realtive;
                overflow:hidden;
                // background:pink;
              }
            

          `}</style>



          <style global jsx>{`
                body {
                  margin:0px !important;
                  padding:0px !important;
                  // overflow: hidden !important;
                }
          `}</style>

      </div>
    )
  }
  
}
