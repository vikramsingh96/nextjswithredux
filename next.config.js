const withCSS = require('@zeit/next-css');
const withSass = require('@zeit/next-sass');
const { parsed: localEnv } = require('dotenv').config();
const webpack = require('webpack');

const commonsChunkConfig = (config, test = /\.css$/) => {
    config.plugins = config.plugins.map(plugin => {
        if (
            plugin.constructor.name === 'CommonsChunkPlugin' &&
            // disable filenameTemplate checks here because they never match
            // (plugin.filenameTemplate === 'commons.js' ||
            //     plugin.filenameTemplate === 'main.js')
            // do check for minChunks though, because this has to (should?) exist
            plugin.minChunks != null
        ) {
            const defaultMinChunks = plugin.minChunks;
            plugin.minChunks = (module, count) => {
                if (module.resource && module.resource.match(test)) {
                    return true;
                }
                return defaultMinChunks(module, count);
            };
        }
        return plugin;
    });
    return config;
};

module.exports = withSass(withCSS({
    distDir: 'build',
    webpack: (config, {dev, isServer }) => {
        if (!dev) {
            config.devtool = 'source-map';
            for (const plugin of config.plugins) {
                if (plugin['constructor']['name'] === 'UglifyJsPlugin') {
                    plugin.options.sourceMap = true;
                    break;
                }
            }
        };
        config = commonsChunkConfig(config, /\.(sass|scss|css)$/);
        config.plugins.push(new webpack.EnvironmentPlugin(localEnv));
        config.node = {
            fs: 'empty'
        };
        return config
    },
}));

