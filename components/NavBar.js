import React, { Component } from 'react';
import Link from 'next/link';

export default class NavBar extends Component {
    render() {
        return (
            <React.Fragment>
                <nav className="navbar">
                    <div className="nav-container">
                        <div className="navbar-brand_container">
                        <Link href="/"><h4 className="navbar-brand">Meet<span>Up</span></h4></Link>
                        </div>

                        <div className="categories_container">
                            <ul>
                              <li className="nav-item"><Link href="/"><a className="nav-link"><span>Home</span></a></Link></li>
                              {/* <li className="nav-item"><a href="#about-section" className="nav-link"><span>About</span></a></li>
                              <li className="nav-item"><a href="#speakers-section" className="nav-link"><span>Speakers</span></a></li>
                              <li className="nav-item"><a href="#schedule-section" className="nav-link"><span>Schedule</span></a></li>
                              <li className="nav-item"><a href="#pricing-section" className="nav-link"><span>Pricing</span></a></li>
                              <li className="nav-item"><a href="#blog-section" className="nav-link"><span>Blog</span></a></li> */}
                              <li className="nav-item"><Link href="/Contact"><a className="nav-link"><span>Contact</span></a></Link></li>
                              {/* <li className="nav-item cta"><a href="#" className="nav-link">Buy Ticket</a></li> */}
                            </ul> 
                        </div>
                    </div>
                </nav>   

                <style jsx>{`
                      .navbar
                      {
                        height: auto;
                        width: 100%;
                        background: ${this.props.background};
                        position:fixed;
                        top:0px;
                        left:0px;
                        right:0px;
                        z-index:100;
                      }
                      .nav-container
                      {
                        height: 80px;
                        width: 100%;
                        position: relative;
                        display:flex;
                        flex-direction: row;
                      }
                      .navbar-brand_container
                      {
                        display:flex;
                        height:auto;
                        width:25%;
                        // background:red;
                        align-items: center;
                        justify-content:center;
                      }

                      .navbar-brand_container .navbar-brand
                      {
                          color: #fff;
                          font-weight: 900;
                          font-size: 24px;
                          cursor:pointer;
                      }

                      .navbar-brand_container .navbar-brand span
                      {
                        color: #fbb901;
                      }
                      


                      .categories_container
                      {
                        display:flex;
                        height:auto;
                        width:75%;
                        align-items:center;
                        justify-content:flex-end;
                        padding-right:25px;
                      }
                      
                      .categories_container .nav-item 
                      {
                          list-style-type:none;
                          display: inline-block;
                          font-size: 16px;
                          padding-top: .7rem;
                          padding-bottom: .7rem;
                          padding-left: 20px;
                          padding-right: 20px;
                          opacity: 1!important;
                      }
                      .categories_container .nav-item a
                      {
                        text-decoration:none;
                        padding-bottom:5px;
                        color:#fff;
                        font-weight: 400;
                        font-size: 15px;
                        font-family: sans-serif;
                      }
                      .categories_container .nav-item a:hover
                      {
                        color:blue;
                      }
                      
                      
                      

                `}</style>
            </React.Fragment>
        )
    }
}
