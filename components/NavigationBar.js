import React, { Component } from 'react';
import { connect } from 'react-redux';
import { HANDLE_GET_NAVIGATIONDATA } from '../redux/actions/actions';

class NavigationBar extends Component {

    static getInitialProps({store}) {}

    componentDidMount() {
        this.props.handleGetNavigatinData();
    }


    render() {
        return (
            <React.Fragment>
                  <div className="navigation_container">
                     <div className="navigation_categories_container">
                        <ul>
                           {
                               this.props.navigationData && this.props.navigationData.map((value,key)=>{
                                   let brandNameandSlugs=[];
                                   let brandNames = value.brands_name.split(',');
                                   let brands_slug = value.brands_slug.split(',');
                                    for(var i=0;i<brandNames.length;i++){
                                        brandNameandSlugs.push({name:brandNames[i], slug:brands_slug[i]});
                                    }

                                   return <li key={key}>
                                       <a href={`https://www.gyftr.com/hdfcsmartbuy/category/${value.slug}`} className="categories_items">
                                           <figure>
                                               <img alt={value.name} src={value.icon_url}/>
                                               <figcaption>{value.name}</figcaption>
                                           </figure>
                                       </a>
                                       <div className="dropDown-categories">
                                           {
                                             brandNameandSlugs.map((v,k)=>{
                                                return <a href={`https://www.gyftr.com/hdfcsmartbuy/${v.slug}`} className="dropdown-item" key={k}>{v.name}</a>
                                             })  
                                           }
                                       </div>                                                              
                                   </li>
                               })
                           }     
                        </ul>
                     </div>
                  </div>
                  <style jsx>{`
                    .navigation_container
                    {
                        display:flex;
                        align-items:center;
                        justify-content:flex-end;
                        height:auto;
                        width:100%;     
                        position: fixed;
                        top: 80px;
                        right: 0;
                        width: 100%;
                        z-index: 100;
                        box-shadow: -16px -16px 38px -17px rgba(0,0,0,.19);
                        opacity:0.8;
                    }
                    .navigation_categories_container
                    {
                        height:auto;
                        width:100%;
                        max-width:1150px;
                        position:relative;
                        background: #ffffff;
                    }
                    .navigation_categories_container ul{
                        list-style: none;
                        padding: 0;
                        margin: 0;
                        font-size: 0;
                        text-align: center;
                        position: relative;
                        vertical-align: bottom;
                    }
                    .navigation_categories_container ul li
                    {
                        position: initial;
                        display: inline-block;
                        vertical-align: bottom;
                    }
                    .navigation_categories_container ul li a
                    {
                        padding: 10px 16px 10px 16px;
                        display: block;
                        color: #777777;
                        font-size: 12px;
                        text-align: center;
                        line-height: 1;
                        font-weight: 600;
                        border-bottom: 2px solid #fff;
                        -webkit-transition:all .3s ease-in-out;
                        text-decoration: none;
                        background-color: transparent;
                        font-family: 'Open Sans', sans-serif;
                    }

                    .navigation_categories_container ul li a figure
                    {
                        padding: 0;
                        margin: 0;
                    }
                    .navigation_categories_container img
                    {
                        vertical-align: middle;
                        border-style: none;
                        user-select: none;
                        max-width: 100%;
                        -webkit-transition:all .3s ease-in-out;
                    }
                    .navigation_categories_container ul li a figure figcaption
                    {
                        padding: 7px 0 0;
                        margin: 0;
                    }
                    .dropDown-categories
                    {
                        column-count: 4;
                        min-width: 100%;
                        border-radius: 0;
                        border: 0;
                        box-shadow: 0 0 4px 2px rgba(0,0,0,.05);
                        z-index: 2;
                        margin: 0;
                        padding: 0;

                        position: absolute;
                        top: 100%;
                        left: 0;
                        display: none;
                        float: left;
                        font-size: 1rem;
                        color: #212529;
                        text-align: left;
                        list-style: none;
                        background-color: #fff;
                        background-clip: padding-box;
                        overflow:hidden;
                    }

                    .dropDown-categories a
                    {
                        text-align: left !important;
                        vertical-align: top;
                        font-size: 12px;
                        border: 0;
                        line-height: 1;
                        padding-top: 9px !important;
                        padding-bottom: 9px !important;
                        -webkit-transition:none;
                        -webkit-column-break-inside: avoid;
                        page-break-inside: avoid;
                        break-inside: avoid;
                        font-family: 'Open Sans', sans-serif;
                    }

                    .dropdown-item{
                        display: block;
                        width: 100%;
                        padding: .25rem 1.5rem !important;
                        clear: both;
                        font-weight: 400;
                        color: #212529;
                        text-align: inherit;
                        white-space: nowrap;
                        background-color: transparent;
                        text-decoration:none;
                    }

                    .dropdown-item :hover
                    {
                        background:#f8f9fa !important;
                        color: #011f79 !important;
                    }

                    .navigation_categories_container ul li .categories_items:hover
                    {
                        color: #011f79;
                        text-decoration: none;
                        border-bottom: 2px solid #ed0707;
                    }
                    .navigation_categories_container ul li:hover>.dropDown-categories
                    {
                        display: block;
                    }


                  `}
                  </style>
            </React.Fragment>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        navigationData: state.navigationData,
        isloading: state.isLoading
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
        handleGetNavigatinData: data => dispatch({ type: HANDLE_GET_NAVIGATIONDATA, data })
    }
  }
  
  
  export default connect(mapStateToProps, mapDispatchToProps)(NavigationBar);