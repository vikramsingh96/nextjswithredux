import { reducer } from '../reducer/Reducer';
import { createStore, applyMiddleware } from 'redux'
import {watcherSaga} from '../saga/AllWatcherSagas';
import createSagaMiddleware from "redux-saga";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(reducer,applyMiddleware(sagaMiddleware));

sagaMiddleware.run(watcherSaga);

export default store;

