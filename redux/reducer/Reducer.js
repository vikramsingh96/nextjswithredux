let initialState={
    userDetails: {},
    navigationData: [],
    isLoading: false
}



export const reducer =(state=initialState,action)=>{
    switch(action.type){

        case "SAVE_USER_DETAILS":
            {
                return {
                    ...state,
                    userDetails: action.data
                }
            }

        case "GET_USER_DETAILS":
            {
                return {
                    ...state,
                    userDetails:state.userDetails
                }
            }    

        case "HANDLE_GET_NAVIGATIONDATA":
            {
                return {
                    ...state,
                    isLoading: true
                }
            }
        case "HANDLE_GET_NAVIGATIONDATA_SUCCESS":
            {
                return {
                    ...state,
                    isLoading: false,
                    navigationData: action.navigationData
                }
            }
        case "HANDLE_GET_NAVIGATIONDATA_FAILURE":
            {
                return {
                    ...state,
                    isLoading: false,
                    navigationData: action.error
                }
            }

            
        default:
            return state;
    }
}