import { call, put } from "redux-saga/effects";
import axios from "axios";
import { HANDLE_GET_NAVIGATIONDATA_SUCCESS, HANDLE_GET_NAVIGATIONDATA_FAILURE } from '../actions/actions';


export function getNavigatinData(url) {
    let headers = {
        'Content-Type': 'application/json',
    };
    return axios({
        method: "get",
        url: url,
        headers
    });
}


export function* navigationDataSaga(action) {
    try {
        const response = yield call(getNavigatinData, 'https://api.gyftr.net/smartbuyapi/hdfc/api/v1/home/categories');
        // console.log("in-saga-doclist---->",response);
        const navigationData = response.data.data;


        // dispatch a success action to the store with the new data
        yield put({ type: HANDLE_GET_NAVIGATIONDATA_SUCCESS, navigationData });
    } catch (error) {
        // dispatch a failure action to the store with the error
        yield put({ type: HANDLE_GET_NAVIGATIONDATA_FAILURE, error });
        //{openNotificationWithIcon('error',"User Registration",error)}
    }
}




