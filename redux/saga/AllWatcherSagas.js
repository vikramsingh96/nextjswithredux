import { takeLatest } from "redux-saga/effects";
import { navigationDataSaga } from './GetNavigationDataSaga';
import {HANDLE_GET_NAVIGATIONDATA} from '../actions/actions';

export function* watcherSaga() {
    yield takeLatest(HANDLE_GET_NAVIGATIONDATA, navigationDataSaga);
}