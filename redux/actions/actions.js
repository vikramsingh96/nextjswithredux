export const HANDLE_GET_NAVIGATIONDATA="HANDLE_GET_NAVIGATIONDATA";
export const HANDLE_GET_NAVIGATIONDATA_SUCCESS="HANDLE_GET_NAVIGATIONDATA_SUCCESS";
export const HANDLE_GET_NAVIGATIONDATA_FAILURE="HANDLE_GET_NAVIGATIONDATA_FAILURE";


export const SAVE_USER_DETAILS="SAVE_USER_DETAILS";
export const GET_USER_DETAILS="GET_USER_DETAILS";